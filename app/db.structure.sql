-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `feed`;
CREATE TABLE `feed` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `product_count` int(12) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `feed_item`;
CREATE TABLE `feed_item` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT,
  `feed_id` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feed_id` (`feed_id`),
  CONSTRAINT `feed_item_ibfk_1` FOREIGN KEY (`feed_id`) REFERENCES `feed` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `feed_item_value`;
CREATE TABLE `feed_item_value` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT,
  `feed_item_id` bigint(64) NOT NULL,
  `key` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `value` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feed_item_id` (`feed_item_id`),
  CONSTRAINT `feed_item_value_ibfk_1` FOREIGN KEY (`feed_item_id`) REFERENCES `feed_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2017-05-17 13:40:29
