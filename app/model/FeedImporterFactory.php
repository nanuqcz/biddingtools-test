<?php
namespace App\Model;

use Nette,
    Nette\DI;


/**
 * FeedImporterFactory
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class FeedImporterFactory
{
    private $context;

    public function __construct(DI\Container $context)
    {
        $this->context = $context;
    }


    public function create()
    {
        return $this->context->createInstance('App\Model\FeedImporter');
    }

}
