<?php
namespace App\Model;

use Nette,
    Nette\Utils\Strings,
    Nette\Utils\FileSystem;


/**
 * FeedDownloader
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class FeedDownloader
{
    private $downloadDir;

    public function __construct($downloadDir)
    {
        $this->downloadDir = $downloadDir;
    }


    public function download($url)
    {
        // Prepare download
        $filename = Strings::webalize($url, '.');
        $downloadPath = "$this->downloadDir/$filename." . date('Y-m-d_H-i-s');

        FileSystem::createDir($this->downloadDir);  // make sure the folder exists

        // Do it!
        file_put_contents(
            $downloadPath,
            fopen($url, 'r')  // TODO exception?
        );

        return $downloadPath;
    }

}
