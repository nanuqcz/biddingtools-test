<?php
namespace App\Model\Exceptions;

use Exception;


/**
 * EntryNotFound
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class EntryNotFound extends Exception
{
}
