<?php
namespace App\Model;

use Nette,
    Nette\Utils\DateTime;


/**
 * FeedManager
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class FeedManager
{
    private $feedImporterFactory;
    private $feedRepository;

    public function __construct(
        FeedImporterFactory $feedImporterFactory,
        FeedRepository $feedRepository
    ){
        $this->feedImporterFactory = $feedImporterFactory;
        $this->feedRepository = $feedRepository;
    }


    public function import($url)
    {
        $importer = $this->feedImporterFactory->create();
        $importer->import($url);
    }


    public function search($conditions = NULL, $limit = NULL, $order = NULL)
    {
        return $this->feedRepository->search($conditions, $limit, $order);
    }


    public function getKeys($feedId)
    {
        return $this->feedRepository->getKeys($feedId);
    }


    public function getItemCount($feedId)
    {
        return $this->feedRepository->getItemCount($feedId);
    }


    public function fetchItems($conditions = NULL, $limit = NULL, $order = NULL)
    {
        return $this->feedRepository->fetchItems($conditions, $limit, $order);
    }

}
