<?php
namespace App\Model;

use Nette,
    Nette\Utils\DateTime;
use Exception;
use SimpleXMLElement;


/**
 * FeedImporter
 * Class that imports XML data from local file. One instance => one import!
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class FeedImporter
{
    private $feedDownloader;
    private $feedParser;
    private $feedRepository;
    private $feedEntity;
    private $itemCount = 0;


    public function __construct(
        FeedDownloader $feedDownloader,
        FeedParser\Parser $feedParser,
        FeedRepository $feedRepository
    ){
        $this->feedDownloader = $feedDownloader;
        $this->feedParser = $feedParser;
        $this->feedRepository = $feedRepository;
    }


    public function import($url)
    {
        $filePath = $this->feedDownloader->download($url);

        $this->createEntity($url);

        $this->feedParser->fetchItems(
            $filePath,
            [$this, 'foundFeedItem']
        );

        $this->finishEntity();
    }


    public function foundFeedItem($itemData)
    {
        set_time_limit(0);  // reset timer
        $itemEntity = $this->feedRepository->addItem($this->feedEntity);

        foreach ($itemData as $key => $value) {
            $value = $this->buildValue($value);

            $this->feedRepository->addItemValue($itemEntity, $key, $value);
        }

        $this->itemCount++;
    }


    private function createEntity($url)
    {
        $this->feedEntity = $this->feedRepository->add([
            'url' => $url,
            'product_count' => 0,
            'date' => new DateTime(),
        ]);
    }


    private function finishEntity()
    {
        $this->feedRepository->update($this->feedEntity, [
            'product_count' => $this->itemCount,
        ]);
    }


    private function buildValue($value)
    {
        $value = is_scalar($value)
            ? $value
            : $value instanceof SimpleXMLElement
                ? (string)$value  // CDATA
                : (array)$value;

        if (is_scalar($value)) {
            return $value;
        }

        if (is_array($value) && count($value) == 1 && key($value) == 0) {
            return $value[0];
        }

        return json_encode($value);
    }

}
