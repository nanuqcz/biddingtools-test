<?php
namespace App\Model;

use Nette,
    Nette\Database,
    Nette\Utils\ArrayHash;


/**
 * FeedRepository
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class FeedRepository
{
    private $db;

    public function __construct(Database\Context $db)
    {
        $this->db = $db;
    }


    private function fetchId($value)
    {
        return is_scalar($value)
            ? $value
            : $value['id'];
    }


    /********************* FEED *********************/


    public function get($id)
    {
        $entity = $this->db->table('feed')
            ->where('id', $id)
            ->fetch();

        if (!$entity) {
            throw new Exceptions\EntryNotFound;
        }

        return $entity;
    }


    public function add($feedData)
    {
        return $this->db->table('feed')
            ->insert($feedData);
    }


    public function update($entity, $data)
    {
        $id = $this->fetchId($entity);

        return $this->db->table('feed')
            ->where('id', $id)
            ->update($data);
    }


    public function search($conditions = NULL, $limit = NULL, $order = NULL)
    {
        $selection = $this->db->table('feed');

        if ($conditions) {
			$selection->where($conditions);
		}

		if ($limit) {
			$selection->limit($limit[0], $limit[1]);
		}

		if ($order) {
			$selection->order($order);
		}

        return $selection;
    }


    public function getItemCount($feedId)
    {
        $selection = $this->db->table('feed_item')
            ->where('feed_id', $feedId);

        return $selection->count('id');
    }


    public function getKeys($feedId)
    {
        $selection = $this->db->table('feed_item_value')
            ->select('DISTINCT key')
            ->where('feed_item.feed_id', $feedId);

        return $selection->fetchPairs('key', 'key');
    }


    /********************* FEED_ITEM *********************/


    public function addItem($feedEntity)
    {
        $feedId = $this->fetchId($feedEntity);

        return $this->db->table('feed_item')
            ->insert([
                'feed_id' => $feedId,
            ]);
    }


    public function searchItems($conditions = NULL, $limit = NULL, $order = NULL)
    {
        $selection = $this->db->table('feed_item');

        if ($conditions) {
			$selection->where($conditions);
		}

		if ($limit) {
			$selection->limit($limit[0], $limit[1]);
		}

		if ($order) {
			$selection->order($order);
		}

        return $selection;
    }


    public function fetchItems($conditions = NULL, $limit = NULL, $order = NULL)
    {
        $selection = $this->searchItems($conditions, $limit, $order);

        $items = [];
        foreach ($selection as $row) {
            $item = ArrayHash::from($row);

            $item->values = $row->related('feed_item_value')
                ->fetchPairs('key', 'value');

            $items[] = $item;
        }

        return $items;
    }


    /********************* FEED_ITEM_VALUE *********************/


    public function addItemValue($itemEntity, $key, $value)
    {
        $itemId = $this->fetchId($itemEntity);

        return $this->db->table('feed_item_value')
            ->insert([
                'feed_item_id' => $itemId,
                'key' => $key,
                'value' => $value,
            ]);
    }

}
