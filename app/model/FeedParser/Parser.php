<?php
namespace App\Model\FeedParser;

use App\Model\FeedParser\Exceptions\UnexpectedXmlStructure,
    App\Model\FeedParser\Exceptions\DriverNotFound;


/**
 * FeedParser\Parser
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class Parser
{
    private $drivers = [];


    public function addDriver(IDriver $driver)
    {
        $this->drivers[] = $driver;
    }


    public function fetchItems($filePath, $callback)
    {
        $parsed = FALSE;

        foreach ($this->drivers as $driver) {
            try {
                $driver->fetchItems($filePath, $callback);

                $parsed = TRUE;
                break;  // driver was OK, no need to try next one
            } catch (UnexpectedXmlStructure $e) {
                // do nothing, just try another driver
            }
        }

        if (!$parsed) {
            throw new DriverNotFound('Driver suitable for this XML structure was not found.');
        }
    }

}
