<?php
namespace App\Model\FeedParser;

use XMLReader;


/**
 * FeedParser\BaseXmlDriver
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
abstract class BaseXmlDriver implements IDriver
{
    protected $reader;
    private $nonElementTypes = [
        XMLReader::TEXT,
        XmlReader::CDATA,
        XmlReader::COMMENT,
        XmlReader::WHITESPACE,
        XmlReader::SIGNIFICANT_WHITESPACE,
        XmlReader::XML_DECLARATION,
    ];


    protected function initReader($filePath)
    {
        $this->reader = new XMLReader();
        $this->reader->open($filePath);
    }


    protected function closeReader()
    {
        $this->reader->close();
    }


    protected function readElement()
    {
        $result = $this->reader->read();

        while (in_array($this->reader->nodeType, $this->nonElementTypes)) {
            $result = $this->reader->next();
        }

        return $result;
    }


    protected function nextElement()
    {
        $result = $this->reader->next();

        while (in_array($this->reader->nodeType, $this->nonElementTypes)) {
            $result = $this->reader->next();
        }

        return $result;
    }


    protected function endElement($elementName)
    {
        do {
            $result = $this->reader->next();
        } while ($this->checkEndElement($elementName));

        return $result;
    }


    protected function checkElement($elementName = NULL, $throwException = FALSE)
    {
        $isOk = TRUE;

        if ($this->reader->nodeType != XMLReader::ELEMENT) {
            $isOk = FALSE;
        }
        if ($elementName && ($this->reader->name != $elementName)) {
            $isOk = FALSE;
        }

        if (!$isOk && $throwException) {
            throw new Exceptions\UnexpectedXmlStructure();
        }

        return $isOk;
    }


    protected function checkEndElement($elementName = NULL, $throwException = FALSE)
    {
        $isOk = TRUE;

        if ($this->reader->nodeType != XMLReader::END_ELEMENT) {
            $isOk = FALSE;
        }
        if ($elementName && ($this->reader->name != $elementName)) {
            $isOk = FALSE;
        }

        if (!$isOk && $throwException) {
            throw new Exceptions\UnexpectedXmlStructure();
        }

        return $isOk;
    }

}
