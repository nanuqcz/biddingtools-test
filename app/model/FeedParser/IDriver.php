<?php
namespace App\Model\FeedParser;

use Nette;


/**
 * FeedParser\IDriver
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
interface IDriver
{

    public function fetchItems($filePath, $callback);

}
