<?php
namespace App\Model\FeedParser;


/**
 * FeedParser\HeurekaDriver
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class HeurekaDriver extends BaseXmlDriver
{

    public function fetchItems($filePath, $itemCallback)
    {
        $this->initReader($filePath);

        // <SHOP>
        $this->readElement();
        $this->checkElement('SHOP', TRUE);

        // <SHOP>/<SHOPITEM>
        while ($this->readElement()) {
            if ($this->checkEndElement('SHOP')) {
                break;
            }
            $this->checkElement('SHOPITEM', TRUE);

            $itemData = simplexml_load_string($this->reader->readOuterXML());
            call_user_func($itemCallback, $itemData);

            $this->endElement('SHOPITEM');
        }

        $this->closeReader();
    }

}
