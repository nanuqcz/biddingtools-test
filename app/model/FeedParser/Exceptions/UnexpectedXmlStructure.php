<?php
namespace App\Model\FeedParser\Exceptions;

use Nette;
use Exception;


/**
 * UnexpectedXmlStructure
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class UnexpectedXmlStructure extends Exception
{
}
