<?php
namespace App\Model\FeedParser\Exceptions;

use Exception;


/**
 * DriverNotFound
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class DriverNotFound extends Exception
{
}
