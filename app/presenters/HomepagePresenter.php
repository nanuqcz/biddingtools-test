<?php
namespace App\Presenters;

use App\Model\FeedManager;
use IPub\VisualPaginator\Components as VisualPaginator;
use Nette,
    Nette\Application\UI\Form,
    Nette\Forms\Controls\Button;


/**
 * Homepage
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class HomepagePresenter extends BasePresenter
{
    /** @inject @var FeedManager */
    public $feedManager;


    public function renderDefault()
    {
        // 1) Get data
        $feeds = $this->feedManager->search(NULL, NULL, 'id DESC');
        $count = $feeds->count();

        // Paginate
        $paginator = $this['paginator']->getPaginator();
        $paginator->itemCount = $count;
        $feedsPaginated = $this->feedManager->search(
            NULL,                                            // conditions
            [$paginator->itemsPerPage, $paginator->offset],  // limit
            'id DESC'                                        // order
        );

        // 2) Render
        $this->template->feeds = $feeds;
        $this->template->feedsPaginated = $feedsPaginated;

        if ($this->isAjax()){
            $this->redrawControl();
        }
    }


    /**
	 * Create items paginator
	 *
	 * @return VisualPaginator\Control
	 */
	protected function createComponentPaginator()
	{
		$control = parent::createComponentPaginator();

		return $control;
	}


    protected function createComponentFeedUrlForm()
    {
        $form = new Form();
        $form->addText('url', 'URL feedu');
        $form->addSubmit('load', 'Načíst feed');

        $form['load']->onClick[] = array($this, 'loadFeedUrlForm');
        return $form;
    }


    public function loadFeedUrlForm(Button $button)
    {
        $form = $button->form;
        $values = $form->values;

        $this->feedManager->import($values->url);

        $this->flashMessage('Feed byl importován.', 'success');
        $this->redirect('this');
    }

}
