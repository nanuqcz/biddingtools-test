<?php
namespace App\Presenters;

use App\Model\FeedManager;
use IPub\VisualPaginator\Components as VisualPaginator;
use Nette;


/**
 * Item
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class ItemPresenter extends BasePresenter
{
	/** @persistent @var int */
	public $itemsPerPage = 10;
	public $columnsPerPage = 5;

	/** @inject @var FeedManager */
	public $feedManager;


	public function renderDefault($id)
	{
		// 1) Get keys
		$keys = $this->feedManager->getKeys($id);
		unset($keys['ITEM_ID']);

		$paginator = $this['columnPaginator']->getPaginator();
		$paginator->itemCount = count($keys);

		$keys = array_slice($keys, $paginator->offset, $paginator->itemsPerPage);

		// 2) Get values
		$count = $this->feedManager->getItemCount($id);
		$paginator = $this['paginator']->getPaginator();
		$paginator->itemCount = $count;

		$itemsPaginated = $this->feedManager->fetchItems(
			['feed_id' => $id],                              // conditions
			[$paginator->itemsPerPage, $paginator->offset],  // limit
			'id ASC'                                         // order
		);

		// 3) Render
		$this->template->keys = $keys;
		$this->template->itemsPaginated = $itemsPaginated;

		if ($this->isAjax()){
			$this->redrawControl();
		}
	}


	/********************* ITEMS PAGINATOR *********************/


	/**
	 * Create items paginator
	 *
	 * @return VisualPaginator\Control
	 */
	protected function createComponentPaginator()
	{
		$control = parent::createComponentPaginator();
		$control->getPaginator()->itemsPerPage = $this->itemsPerPage;

		return $control;
	}


	public function handleSetItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;

		$this->redirect('this');
	}


	/********************* COLUMN PAGINATOR *********************/



	protected function createComponentColumnPaginator()
	{
		$control = parent::createComponentPaginator();
		$control->getPaginator()->itemsPerPage = $this->columnsPerPage;

		$control->setTemplateFile(__DIR__ . '/templates/Item/column-paginator.latte');

		return $control;
	}

}
