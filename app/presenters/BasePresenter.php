<?php
namespace App\Presenters;

use App\Model\FeedManager;
use IPub\VisualPaginator\Components as VisualPaginator;
use Nette,
    Nette\Application\UI\Form,
    Nette\Forms\Controls\Button;


/**
 * Base
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class BasePresenter extends Nette\Application\UI\Presenter
{

    /**
	 * Create items paginator
	 *
	 * @return VisualPaginator\Control
	 */
	protected function createComponentPaginator()
	{
		// Init visual paginator
		$control = new VisualPaginator\Control;
		$control->getPaginator()->itemsPerPage = 10;

		// To use bootstrap default template
		$control->setTemplateFile('bootstrap.latte');

		return $control;
	}

}
